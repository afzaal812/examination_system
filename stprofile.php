<?php include("connect.php");
session_start();
if(empty($_SESSION['st_id'])){header("location:index.php");}
$id=$_SESSION['st_id'];
if($_GET['update']=='success'){
	$message1="<div style='width:95%;margin-left:5px; padding:6px; background-color:#D2FFD2; color:#49704D; border:#999 1px solid; text-align:center'><img src='images/accept.png'/>&nbsp;Profile Edited Successfully </div>";
}
$qry=mysql_query("select * from aup_students where st_id=$id");
while($row=mysql_fetch_array($qry)){
$st_name=$row['st_name'];
$st_fname=$row['st_fname'];
$dept_id=$row['dept_id'];
$st_semester=$_SESSION['st_semester']=$row['st_semester'];
$st_reg=$row['st_reg'];
$st_rollno=$_SESSION['st_rollno']=$row['st_rollno'];
$st_gender=$row['st_gender'];
$st_dob=$row['st_dob'];
$email=$row['email'];
$st_address=$row['st_address'];
$st_mob=$row['st_mob'];
$st_status=$row['st_status'];
$st_photo=$_SESSION['st_photo']=$row['st_photo'];
}
$qr=mysql_query("select * from aup_departments where dept_id=$dept_id");
while($row1=mysql_fetch_array($qr)){
	$dept=$_SESSION['dept_name']=$row1['dept_name'];
}
if($st_status==0){
	$link="<a href='updatestatus.php?status=sactive&st_id=$id&st_name=$st_name&dept_id=$dept_id&semester=$st_semester'>Activate</a>";
	$status="Not Active";
} else {
	$link="<a href='updatestatus.php?status=sdeactive&st_id=$id&st_name=$st_name&dept_id=$dept_id&semester=$st_semester'>Deactivate</a>";
	$status="Active";
	}
?><!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Profile</title>
<link rel="stylesheet" type="text/css" href="css/login.css">
<link rel="stylesheet" type="text/css" href="css/main.css" />
</head>
<body>
<div id="wrapper">
<?php include("sthead.html");?>
<div id="subwrapper">
  <div class="mainp">
  <?php echo $message1 ; ?>
    <table align="left" cellspacing="5" class="reg">
      <tr>
        <td width="33%">Profile Picture</td>
        <td width="33%"><img src="<?php echo $st_photo ; ?>" alt="" name="" width="154" height="128"></td>
        <td width="34%"><a href="stupdate.php?id=<?php echo $id ; ?>">Edit Profile --></a></td>
      </tr>
      <tr>
        <td>Name</td>
        <td colspan="2"><?php echo $st_name ; ?></td>
      </tr>
      <tr>
        <td>Father Name</td>
        <td colspan="2"><?php echo $st_fname ; ?></td>
      </tr>
      <tr>
        <td>Department</td>
        <td colspan="2"><?php echo $dept ; ?></td>
      </tr>
      <tr>
        <td>Semester</td>
        <td colspan="2"><?php echo $st_semester ; ?></td>
      </tr>
      <tr>
        <td>Registration</td>
        <td colspan="2"><?php echo $st_reg ; ?></td>
      </tr>
      <tr>
        <td>Roll No</td>
        <td colspan="2"><?php echo $st_rollno ; ?></td>
      </tr>
      <tr>
        <td>Gender</td>
        <td colspan="2"><?php echo $st_gender ; ?></td>
      </tr>
      <tr>
        <td>Date of Birth</td>
        <td colspan="2"><?php echo $st_dob ; ?></td>
      </tr>
      <tr>
        <td>Email</td>
        <td colspan="2"><?php echo $email ; ?></td>
      </tr>
      <tr>
        <td>Address</td>
        <td colspan="2"><?php echo $st_address ; ?></td>
      </tr>
      <tr>
        <td>Mobile No</td>
        <td colspan="2"><?php echo $st_mob ; ?></td>
      </tr>
      <tr>
        <td>Account Status</td>
        <td colspan="2"><?php echo $status ; ?></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td colspan="2">&nbsp;</td>
      </tr>
    </table>
  </div><?php include("sidebar.php");?></div>
<?php include("footer.html")?></div></body></html>