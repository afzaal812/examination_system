-- phpMyAdmin SQL Dump
-- version 3.5.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 21, 2013 at 01:58 AM
-- Server version: 5.1.68-cll
-- PHP Version: 5.3.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fisitcom_aup`
--

-- --------------------------------------------------------

--
-- Table structure for table `aup_admin`
--

CREATE TABLE IF NOT EXISTS `aup_admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_name` char(30) DEFAULT NULL,
  `admin_username` varchar(30) DEFAULT NULL,
  `admin_password` varchar(32) DEFAULT NULL,
  `dept_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `admin_gender` varchar(10) DEFAULT NULL,
  `admin_email` varchar(50) DEFAULT NULL,
  `admin_mob` varchar(20) DEFAULT NULL,
  `admin_photo` varchar(250) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `admin_status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`admin_id`),
  UNIQUE KEY `admin_username` (`admin_username`),
  KEY `FK_aup_admin` (`dept_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `aup_admin`
--

INSERT INTO `aup_admin` (`admin_id`, `admin_name`, `admin_username`, `admin_password`, `dept_id`, `subject_id`, `admin_gender`, `admin_email`, `admin_mob`, `admin_photo`, `status`, `admin_status`) VALUES
(15, 'Arbab Wasim Abbas', 'arbab', '823685650277fdcba36c0176d6e15529', 3, 5, 'male', 'fzl.ahmad@gmail.com', '03013434556', 'admin_photo/protected_Jellyfish.jpg', 1, 1),
(18, 'usman anwar', 'usman', '2f1fed5365c79d8fea7859dcc8788d77', 3, 2, 'male', 'usman@yahoo.com', '03013434556', 'admin_photo/protected_Hydrangeas.jpg', 1, 0),
(19, 'Dilawar shah', 'dilawar', '73ca57e5bd7765f3d9481c5f0e75fc18', 3, 2, 'male', 'afzaal.ahmad49@gmail.com', '03013434556', 'admin_photo/protected_Jellyfish.jpg', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `aup_answers`
--

CREATE TABLE IF NOT EXISTS `aup_answers` (
  `sess_id` varchar(100) DEFAULT NULL,
  `st_id` int(11) DEFAULT NULL,
  `quest_desc` text,
  `quest_ans1` text,
  `quest_ans2` text,
  `quest_ans3` text,
  `quest_ans4` text,
  `answer_true` varchar(100) DEFAULT NULL,
  `selected_answer` varchar(100) DEFAULT NULL,
  `subject_name` varchar(40) DEFAULT NULL,
  `quest_id` int(11) DEFAULT NULL,
  KEY `FK_aup_answers` (`st_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aup_answers`
--

INSERT INTO `aup_answers` (`sess_id`, `st_id`, `quest_desc`, `quest_ans1`, `quest_ans2`, `quest_ans3`, `quest_ans4`, `answer_true`, `selected_answer`, `subject_name`, `quest_id`) VALUES
('e7126799eb010912d5dd88a6c3fece2d', 53, 'java is object oriented', 'not', 'ye', 'little', 'fully', '4', '4', 'java-I', 10),
('e7126799eb010912d5dd88a6c3fece2d', 53, 'java float data type is of how many bits?', '3', '2', '4', '8', '3', '3', 'java-I', 5),
('7e7b27e1e0be077ce48ea239ecf8ee2d', 53, 'java support polymorphism', 'yes', 'no', 'little', 'not known', '1', '1', 'java-I', 9),
('7e7b27e1e0be077ce48ea239ecf8ee2d', 53, 'hello java', 'first program', 'second program', '3rd progaram', '4th progam', '1', '1', 'java-I', 8),
('7e7b27e1e0be077ce48ea239ecf8ee2d', 53, 'java has operator class', 'no', 'yes', 'wilbe', 'i think', '3', '2', 'java-I', 12),
('7e7b27e1e0be077ce48ea239ecf8ee2d', 53, 'can we develpe  online applicatio in java?', 'yes', 'no', 'a little bit', 'fully', '4', '1', 'java-I', 13),
('7e7b27e1e0be077ce48ea239ecf8ee2d', 53, 'java has final class', 'yes', 'no', 'i think there is a final class that is responsible for this so there for it is neccessary\r\n to have such type of classes ,make the advance progrming feasible and to restrict unwanted activities.', 'fully', '1', '1', 'java-I', 11);

-- --------------------------------------------------------

--
-- Table structure for table `aup_departments`
--

CREATE TABLE IF NOT EXISTS `aup_departments` (
  `dept_id` int(11) NOT NULL AUTO_INCREMENT,
  `dept_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`dept_id`),
  UNIQUE KEY `dept_name` (`dept_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `aup_departments`
--

INSERT INTO `aup_departments` (`dept_id`, `dept_name`) VALUES
(4, 'Bussiness Administration '),
(17, 'Economics'),
(7, 'Horticulture'),
(3, 'Information Technology');

-- --------------------------------------------------------

--
-- Table structure for table `aup_dropbox`
--

CREATE TABLE IF NOT EXISTS `aup_dropbox` (
  `drop_id` int(11) NOT NULL AUTO_INCREMENT,
  `st_id` int(11) DEFAULT NULL,
  `quest_desc` text,
  `quest_ans1` text,
  `quest_ans2` text,
  `quest_ans3` text,
  `quest_ans4` text,
  `quest_correct` varchar(100) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `sess_id` varchar(100) DEFAULT NULL,
  `quest_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`drop_id`),
  KEY `FK_aup_dropbox` (`st_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `aup_dropbox`
--

INSERT INTO `aup_dropbox` (`drop_id`, `st_id`, `quest_desc`, `quest_ans1`, `quest_ans2`, `quest_ans3`, `quest_ans4`, `quest_correct`, `subject_id`, `sess_id`, `quest_id`) VALUES
(1, 53, 'java support inheritance ?', 'yes', 'no', 'somtime', 'everytime', '1', 2, 'e53b86b16afbac175ec37a64edb0eb29', 4),
(2, 53, 'hello java', 'first program', 'second program', '3rd progaram', '4th progam', '1', 2, 'e53b86b16afbac175ec37a64edb0eb29', 8),
(3, 53, 'java support polymorphism', 'yes', 'no', 'little', 'not known', '1', 2, 'e53b86b16afbac175ec37a64edb0eb29', 9),
(4, 53, 'java has final class', 'yes', 'no', 'i think there is a final class that is responsible for this so there for it is neccessary\r\n to have such type of classes ,make the advance progrming feasible and to restrict unwanted activities.', 'fully', '1', 2, 'e53b86b16afbac175ec37a64edb0eb29', 11),
(6, 53, 'java float data type is of how many bits?', '3', '2', '4', '8', '3', 2, '7e7b27e1e0be077ce48ea239ecf8ee2d', 5);

-- --------------------------------------------------------

--
-- Table structure for table `aup_questions`
--

CREATE TABLE IF NOT EXISTS `aup_questions` (
  `quest_id` int(11) NOT NULL AUTO_INCREMENT,
  `quest_desc` text,
  `quest_ans1` text,
  `quest_ans2` text,
  `quest_ans3` text,
  `quest_ans4` text,
  `quest_correct` varchar(100) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`quest_id`),
  KEY `FK_aup_questions` (`subject_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `aup_questions`
--

INSERT INTO `aup_questions` (`quest_id`, `quest_desc`, `quest_ans1`, `quest_ans2`, `quest_ans3`, `quest_ans4`, `quest_correct`, `subject_id`) VALUES
(3, 'In c++ integer data type is of how may bit size ?', '2', '4', '8', '16', '4', 1),
(4, 'java support inheritance ?', 'yes', 'no', 'somtime', 'everytime', '1', 2),
(5, 'java float data type is of how many bits?', '3', '2', '4', '8', '3', 2),
(6, 'pp deals with?', 'your professional life', 'your home life', 'your hostel life', 'your college life', '1', 1),
(7, 'what is prject management?', 'its is concerned with human management', 'in this we study only project risk', 'we study all the aspect of project', 'none', '3', 5),
(8, 'hello java', 'first program', 'second program', '3rd progaram', '4th progam', '1', 2),
(9, 'java support polymorphism', 'yes', 'no', 'little', 'not known', '1', 2),
(10, 'java is object oriented', 'not', 'ye', 'little', 'fully', '4', 2),
(11, 'java has final class', 'yes', 'no', 'i think there is a final class that is responsible for this so there for it is neccessary\r\n to have such type of classes ,make the advance progrming feasible and to restrict unwanted activities.', 'fully', '1', 2),
(12, 'java has operator class', 'no', 'yes', 'wilbe', 'i think', '3', 2),
(13, 'can we develpe  online applicatio in java?', 'yes', 'no', 'a little bit', 'fully', '4', 2),
(14, 'java support polymorphism ,if it then upto what level?', 'fully', 'little', 'no', 'as you want', '1', 2);

-- --------------------------------------------------------

--
-- Table structure for table `aup_result`
--

CREATE TABLE IF NOT EXISTS `aup_result` (
  `result_id` int(12) NOT NULL AUTO_INCREMENT,
  `sess_id` varchar(100) DEFAULT NULL,
  `st_id` int(11) DEFAULT NULL,
  `subject_name` varchar(40) DEFAULT NULL,
  `score` float DEFAULT NULL,
  `dept_name` varchar(40) DEFAULT NULL,
  `semester_name` varchar(10) DEFAULT NULL,
  `st_rollno` int(8) DEFAULT NULL,
  `percentage` float DEFAULT NULL,
  `start_time` varchar(15) DEFAULT NULL,
  `end_time` varchar(15) DEFAULT NULL,
  `time_consume` varchar(15) DEFAULT NULL,
  `total_true` int(5) DEFAULT NULL,
  `total_wrong` int(5) DEFAULT NULL,
  `total_attemp` int(5) DEFAULT NULL,
  `gpa` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`result_id`),
  KEY `FK_aup_result` (`st_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `aup_students`
--

CREATE TABLE IF NOT EXISTS `aup_students` (
  `st_id` int(11) NOT NULL AUTO_INCREMENT,
  `st_name` char(30) DEFAULT NULL,
  `st_fname` char(30) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `st_password` varchar(32) DEFAULT NULL,
  `dept_id` int(11) DEFAULT NULL,
  `st_semester` varchar(10) DEFAULT NULL,
  `st_reg` varchar(100) NOT NULL,
  `st_rollno` int(5) DEFAULT NULL,
  `st_gender` char(6) DEFAULT NULL,
  `st_dob` date DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `st_address` varchar(150) DEFAULT NULL,
  `st_mob` varchar(15) DEFAULT NULL,
  `st_photo` varchar(150) DEFAULT NULL,
  `st_status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`st_id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `st_reg` (`st_reg`),
  KEY `FK_aup_students` (`dept_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=62 ;

--
-- Dumping data for table `aup_students`
--

INSERT INTO `aup_students` (`st_id`, `st_name`, `st_fname`, `username`, `st_password`, `dept_id`, `st_semester`, `st_reg`, `st_rollno`, `st_gender`, `st_dob`, `email`, `st_address`, `st_mob`, `st_photo`, `st_status`) VALUES
(53, 'Asif Ali', 'Ziarat Khan', 'afzaal', '61243c7b9a4022cb3f8dc3106767ed12', 3, '3RD', '2009-Agr-U-20033', 1, 'male', '1989-03-15', 'fzl.ahmad@gmail.com', 'BELONG TO SWABIBELONG TO SWABI', '03337323282', 'st_photo/protected_Desert.jpg', 0),
(56, 'Noor Ullah Khan', 'Abdur Rashid', 'khan', '6f984575ca0bd1073d5db2b02250e019', 3, '3RD', '2009-Agr-U-20041', 9, 'male', '1990-02-17', 'fzl.ahmad@gmail.com', 'BELONG TO SWABI', '03129626206', 'st_photo/protected_Tulips.jpg', 1),
(57, 'salman', 'khan', 'salman', '03346657feea0490a4d4f677faa0583d', 3, '8TH', '2009-Agr-U-20042', 10, 'male', '1990-02-17', 'sayel@gmail.com', 'peshwar', '03333333333', 'st_photo/protected_Jellyfish.jpg', 1),
(59, 'zubair Khan', 'khan kaka', 'zubair', '08fc70b4b9916f75309d7c0c9a4f5237', 3, '8TH', '2009-Agr-U-20043', 14, 'male', '1988-09-09', 'fzl.ahmad@gmail.com', 'belong to swabi', '03333333333', 'st_photo/protected_Lighthouse.jpg', 0),
(61, 'Sohail Khan', 'Akbar Khan', 'shohail', 'a2de32da8c7b9de7332c15c194ce20df', 3, '3RD', '2009-Agr-U-20046', 6, 'male', '1989-04-14', 'fzl.ahmad@gmail.com', 'belong to mardan', '03445655788', 'st_photo/protected_CIMG8962.JPG', 0);

-- --------------------------------------------------------

--
-- Table structure for table `aup_subject`
--

CREATE TABLE IF NOT EXISTS `aup_subject` (
  `subject_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_name` varchar(50) DEFAULT NULL,
  `dept_id` int(11) DEFAULT NULL,
  `subject_semester` varchar(10) DEFAULT NULL,
  `total_quest` int(5) DEFAULT NULL,
  `total_time` int(5) DEFAULT NULL,
  `marks_per` float DEFAULT NULL,
  `subject_status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`subject_id`),
  UNIQUE KEY `subject_name` (`subject_name`),
  KEY `FK_aup_subject` (`dept_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `aup_subject`
--

INSERT INTO `aup_subject` (`subject_id`, `subject_name`, `dept_id`, `subject_semester`, `total_quest`, `total_time`, `marks_per`, `subject_status`) VALUES
(1, 'c', 3, '3RD', 2, 2, 2, 0),
(2, 'java-I', 3, '3RD', 9, 9, 2, 0),
(4, 'Introduction to Bussiness', 4, '5TH', 2, 20, 2, 1),
(5, 'Professional Practice', 3, '8TH', 6, 6, 6, 0),
(7, 'Digital Signal Proccessing', 3, '8TH', 3, 5, 2, 1),
(11, 'Professional practice1', 3, '8TH', 7, 7, 7, 0);

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE IF NOT EXISTS `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`id`, `name`) VALUES
(1, '0000-00-00'),
(2, '0000-00-00'),
(3, '2012-11-30'),
(4, '0000-00-00'),
(5, '0000-00-00'),
(6, '0000-00-00'),
(7, '0000-00-00'),
(8, '0000-00-00'),
(9, '0000-00-00'),
(10, '0000-00-00'),
(11, '0000-00-00'),
(12, '0000-00-00'),
(13, '0000-00-00'),
(14, '0000-00-00'),
(15, '0000-00-00'),
(16, '0000-00-00'),
(17, '0000-00-00'),
(18, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `timeman`
--

CREATE TABLE IF NOT EXISTS `timeman` (
  `time_id` int(11) NOT NULL AUTO_INCREMENT,
  `st_id` int(11) DEFAULT NULL,
  `start_time` varchar(15) DEFAULT NULL,
  `consume_time` varchar(15) DEFAULT NULL,
  `remain_time` varchar(15) DEFAULT NULL,
  `sess_id` varchar(100) DEFAULT NULL,
  `new_sessid` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`time_id`),
  KEY `FK_timeman` (`st_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `timeman`
--

INSERT INTO `timeman` (`time_id`, `st_id`, `start_time`, `consume_time`, `remain_time`, `sess_id`, `new_sessid`) VALUES
(5, 56, '1360163235', '81', '399', 'a72206190cd6e84b7a36bcef19593f73', ''),
(6, 53, '1361895445', '86', '454', 'e53b86b16afbac175ec37a64edb0eb29', '4e3fa14f0f229b9c72ff433b836a6c33'),
(7, 53, '1362239915', '33', '507', 'e7126799eb010912d5dd88a6c3fece2d', '5b9f20b7b902ec1850e5f77fc3cfc997'),
(8, 53, '1364016170', '116', '424', '7e7b27e1e0be077ce48ea239ecf8ee2d', '25b0e32e272a20da7a7da820d9c0df07');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `aup_admin`
--
ALTER TABLE `aup_admin`
  ADD CONSTRAINT `FK_aup_admin` FOREIGN KEY (`dept_id`) REFERENCES `aup_departments` (`dept_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `aup_answers`
--
ALTER TABLE `aup_answers`
  ADD CONSTRAINT `FK_aup_answers` FOREIGN KEY (`st_id`) REFERENCES `aup_students` (`st_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `aup_dropbox`
--
ALTER TABLE `aup_dropbox`
  ADD CONSTRAINT `FK_aup_dropbox` FOREIGN KEY (`st_id`) REFERENCES `aup_students` (`st_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `aup_questions`
--
ALTER TABLE `aup_questions`
  ADD CONSTRAINT `FK_aup_questions` FOREIGN KEY (`subject_id`) REFERENCES `aup_subject` (`subject_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `aup_result`
--
ALTER TABLE `aup_result`
  ADD CONSTRAINT `FK_aup_result` FOREIGN KEY (`st_id`) REFERENCES `aup_students` (`st_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `aup_students`
--
ALTER TABLE `aup_students`
  ADD CONSTRAINT `FK_aup_students` FOREIGN KEY (`dept_id`) REFERENCES `aup_departments` (`dept_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `aup_subject`
--
ALTER TABLE `aup_subject`
  ADD CONSTRAINT `FK_aup_subject` FOREIGN KEY (`dept_id`) REFERENCES `aup_departments` (`dept_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `timeman`
--
ALTER TABLE `timeman`
  ADD CONSTRAINT `FK_timeman` FOREIGN KEY (`st_id`) REFERENCES `aup_students` (`st_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
