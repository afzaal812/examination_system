<?php
session_start();
$string = '';
for ($i = 0; $i <8 ; $i++) {
	// this numbers refer to numbers of the ascii table (lower case)
	$string .= chr(rand(100, 122));
}

$_SESSION['rand_code'] = $string;

$dir = 'fonts/';

$image = imagecreatetruecolor(230, 80);
$black = imagecolorallocate($image, 0, 0, 0);
$color = imagecolorallocate($image, 00, 66, 00); // red
$white = imagecolorallocate($image, 255, 255, 255);

imagefilledrectangle($image,0,0,399,99,$white);
for ($x1=1; $x<=30; $x++){
	$x1 = rand(5,200);
	$y1 = rand(1, 100);
	$x2 = rand(5, 200);
	$y2 = rand(1, 100);
	imageline($image, $x1, $y1, $x2, $y2, $text_color);
	}

imagettftext ($image, 30, 0, 10, 40, $color,"tahomabd.ttf", $_SESSION['rand_code']);

header("Content-type:image/png");
imagepng($image);

?>